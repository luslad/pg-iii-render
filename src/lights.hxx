#pragma once

#include <vector>
#include <cmath>
#include "math.hxx"
#include "rng.hxx"

class AbstractLight
{
public:

	virtual Vec3f sampleIllumination(const Vec3f& aSurfPt, const Frame& aFrame, Vec3f& oWig, float& oLightDist, const Vec2f& samples) const
	{
		return Vec3f(0);
	}

	virtual Vec3f emission() const
	{
		return Vec3f(0);
	}

	virtual float pdfW(float dist, float cosThetaY) const
	{
		return 0.f;
	}

	virtual bool isDelta() const { return false; }
};

//////////////////////////////////////////////////////////////////////////
class AreaLight : public AbstractLight
{
public:

    AreaLight(
        const Vec3f &aP0,
        const Vec3f &aP1,
        const Vec3f &aP2)
    {
        p0 = aP0;
		p1 = aP1;
		p2 = aP2;
        e1 = aP1 - aP0;
        e2 = aP2 - aP0;

        Vec3f normal = Cross(e1, e2);
        float len    = normal.Length();
        mArea = len * 0.5f;
        mInvArea = 1.f / len * 0.5f;
        mFrame.SetFromZ(normal);
    }

    Vec3f sampleIllumination(const Vec3f& aSurfPt, const Frame& aFrame,
                             Vec3f& oWig, float& oLightDist, const Vec2f& samples) const override
    {
		auto barycentric = SampleUniformTriangle(samples);
		float u0 = barycentric.x, u1 = barycentric.y;
    	
		auto position = p0 + u0 * e1 + u1 * e2;

		oWig = position - aSurfPt;
		float distSqr = oWig.LenSqr();
		oLightDist = sqrt(distSqr);

		oWig /= oLightDist;

		float cosSurf = Dot(aFrame.mZ, oWig);
		float cosLight = Dot(mFrame.mZ, Normalize(aSurfPt - position));
    	
		if (cosSurf <= 0.f || cosLight <= 0.f)
			return Vec3f(0);

		return mRadiance * (cosSurf * cosLight) / distSqr * mArea;
    }

    Vec3f emission() const override
    {
		return mRadiance;
    }

    float pdfW(float dist, float cosThetaY) const override
    {
		return mInvArea * Sqr(dist) / cosThetaY;
    }
	
    Vec3f p0,p1, p2, e1, e2;
    Frame mFrame;
    Vec3f mRadiance;
    float mArea;
    float mInvArea;
};

//////////////////////////////////////////////////////////////////////////
class PointLight : public AbstractLight
{
public:

    PointLight(const Vec3f& aPosition)
    {
        mPosition = aPosition;
    }


    Vec3f sampleIllumination(const Vec3f& aSurfPt, const Frame& aFrame, Vec3f& oWig, float& oLightDist,
	    const Vec2f& samples) const override
	{
		oWig           = mPosition - aSurfPt;
		float distSqr  = oWig.LenSqr();
		oLightDist     = sqrt(distSqr);
		
		oWig /= oLightDist;

		float cosTheta = Dot(aFrame.mZ, oWig);

		if(cosTheta <= 0)
			return Vec3f(0);

		return mIntensity * cosTheta / distSqr;
	}


	bool isDelta() const override { return true; }

    Vec3f mPosition;
    Vec3f mIntensity;
};


//////////////////////////////////////////////////////////////////////////
class BackgroundLight : public AbstractLight
{
public:
    BackgroundLight()
    {
        mBackgroundColor = Vec3f(135, 206, 250) / Vec3f(255.f);
    }

	Vec3f sampleIllumination(const Vec3f& aSurfPt, const Frame& aFrame,
		Vec3f& oWig, float& oLightDist, const Vec2f& samples) const override
	{    	
		float pdfW;
		auto direction = SampleCosHemisphereW(samples, &pdfW);

		direction = aFrame.ToWorld(direction);
    	
		oWig = Normalize(direction);
		oLightDist = std::numeric_limits<float>::max();

		float cosTheta = Dot(aFrame.mZ, oWig);

		if (cosTheta <= 0)
			return Vec3f(0);
    	
		return mBackgroundColor * cosTheta / pdfW;

	}


	Vec3f emission() const override
	{
		return mBackgroundColor;
	}

    float pdfW(float dist, float cosThetaY) const override
    {
		return 0.f;
    }
	
    Vec3f mBackgroundColor;
};
