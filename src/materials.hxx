#pragma once

#include "math.hxx"
#include "rng.hxx"

class Material {
public:
	Material()
	{
		Reset();
	}

	void Reset()
	{
		mDiffuseReflectance = Vec3f(0);
		mPhongReflectance = Vec3f(0);
		mPhongExponent = 1.f;
	}

	Vec3f evalBrdf(const Vec3f& wil, const Vec3f& wol) const
	{
		if (wil.z <= 0 && wol.z <= 0) return Vec3f(0);

		Vec3f diffuseComponent = evalBrdfDiffuse();
		Vec3f glossyComponent = evalBrdfSpecular(wil, wol);

		return diffuseComponent + glossyComponent;
	}


	Vec3f evalBrdfDiffuse() const
	{
		return mDiffuseReflectance / PI_F;
	}

	Vec3f evalBrdfSpecular(const Vec3f& wil, const Vec3f& wol) const
	{

		//reflective vector
		const Vec3f r = ReflectLocal(wil);
		float cosThetaReflection = Dot(r, wol);

		return (mPhongExponent + 2.f) / (2.f * PI_F) *
			mPhongReflectance *
			std::powf(std::max(cosThetaReflection, 0.f), mPhongExponent);
	}

	Vec3f sampleDirL(const Vec3f& incDirL, Rng& rng) const
	{
		float pd = mDiffuseReflectance.Max();
		float ps = mPhongReflectance.Max();

		pd /= (pd + ps); // prob of choosing the diffuse component
		ps /= (pd + ps); // prob of choosing the specular comp.

		Vec3f genDir;
		float pdfDiff = 0.f, pdfSpec = 0.f;
		if (rng.GetFloat() <= pd) genDir = sampleDirUniform(rng.GetVec2f(), nullptr);
		else genDir = sampleDirSpecular(incDirL, rng.GetVec2f(), nullptr);

		return genDir;
	}


	//local coords
	Vec3f sampleDirUniform(const Vec2f& samples, float* oPdfW) const
	{
		return SampleCosHemisphereW(samples, oPdfW);
	}

	//local coords
	Vec3f sampleDirSpecular(const Vec3f& incDirL, const Vec2f& samples, float* oPdfW) const
	{
		// build a local coordinate frame with ideal reflected direction = z-axi
		Frame lobeFrame;
		const auto reflDirL = ReflectLocal(incDirL);
		lobeFrame.SetFromZ(reflDirL);

		// generate direction in the lobe coordinate frame
		auto dirInLobeFrame = SamplePowerCosHemisphereW(samples, mPhongExponent, oPdfW);
		auto dir = lobeFrame.ToWorld(dirInLobeFrame);

		return dir;
	}

	float evalPdf(const Vec3f& wil, const Vec3f wol) const
	{
		float probDiffuse = mDiffuseReflectance.Max();
		float probSpecular = mPhongReflectance.Max();
		auto normalization = 1.f / (probDiffuse + probSpecular);

		probDiffuse *= normalization; // prob of choosing the diffuse component
		probSpecular *= normalization; // prob of choosing the specular comp.

		const Vec3f wrl = ReflectLocal(wol);
		Frame lobeFrame;
		lobeFrame.SetFromZ(wrl);
		const Vec3f dirInLobe = lobeFrame.ToLocal(wil);

		return probDiffuse * evalDiffusePdf(wil) +
			probSpecular * evalSpecularPdf(dirInLobe);
	}

	float evalDiffusePdf(const Vec3f& wil) const
	{
		return wil.z * INV_PI_F;
	}

	float evalSpecularPdf(const Vec3f& wil) const
	{
		return PowerCosHemispherePdfW(Vec3f(0, 0, 1), wil, mPhongExponent);
	}


	Vec3f mDiffuseReflectance;
	Vec3f mPhongReflectance;
	float mPhongExponent;
};
