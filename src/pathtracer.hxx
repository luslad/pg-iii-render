#pragma once

#include <vector>
#include <cmath>
#include <omp.h>
#include <cassert>
#include "renderer.hxx"
#include "rng.hxx"

// Right now this is a copy of EyeLight renderer. The task is to change this 
// to a full-fledged path tracer.
class PathTracer : public AbstractRenderer
{
public:

	PathTracer(
		const Scene& aScene,
		int aSeed = 1234
	) :
		AbstractRenderer(aScene), mRng(aSeed)
	{
	}



	virtual void RunIteration(int aIteration)
	{
		const int resX = int(mScene.mCamera.mResolution.x);
		const int resY = int(mScene.mCamera.mResolution.y);

		for (int pixID = 0; pixID < resX * resY; pixID++)
		{
			//////////////////////////////////////////////////////////////////////////
			// Generate ray
			const int x = pixID % resX;
			const int y = pixID / resX;

			const Vec2f sample = Vec2f(float(x), float(y)) + mRng.GetVec2f();

			Ray ray = mScene.mCamera.GenerateRay(sample);

			auto pixelColor = estimateLinMis(ray);
			mFramebuffer.AddColor(sample, pixelColor);	
		}

		mIterations++;
	}

	Vec3f estimateLinMis(Ray ray)
	{
		Vec3f accum(0.f, 0.f, 0.f);
		Vec3f throughput = Vec3f(1.f);
		for (std::size_t bounces = 0; bounces < 20; ++bounces)
		{
			Isect isect;
			isect.dist = 1e36f;

			if (mScene.Intersect(ray, isect))
			{
				if (isect.lightID >= 0)
				{
					const AbstractLight* light = mScene.GetLightPtr(isect.lightID);
					if (bounces == 0) accum = accum + throughput * light->emission();
					break;
				}

				Frame frame;
				frame.SetFromZ(isect.normal);
				const auto& mat = mScene.GetMaterial(isect.matID);
				const Vec3f surfPt = ray.org + ray.dir * isect.dist;
				const auto wol = frame.ToLocal(-ray.dir);

				//sample light and get next direction
				Vec3f wil;
				accum = accum + throughput * sampleMis(frame, surfPt, wol, mat, &wil);

				ray = Ray(surfPt, frame.ToWorld(wil), EPS_RAY);

				//adjust troughput
				const auto cos = Dot(Normalize(wil), { 0, 0, 1 });
				Vec3f throughputWeights = mat.evalBrdf(wil, wol) * cos * (1.f / mat.evalPdf(wil, wol));
				throughput = throughput * throughputWeights;


				//russian roulette
				const auto survivalProb = std::min(1.f, throughput.Max());
				if (mRng.GetFloat() < survivalProb)
				{
					throughput /= survivalProb;
				}
				else
				{
					break;
				}
			}
			else
			{
				break;
			}
		}

		return accum;
	}


	Vec3f sampleMis(Frame surfaceFrame, const Vec3f surfPt, const Vec3f wol, const Material& mat, Vec3f* oWil = nullptr)
	{
		Vec3f LoDirect(0.f);
		
		//sample brdf
		Vec3f wilB;
		const Vec3f LoBRDF = sampleBrdf(surfaceFrame, surfPt, mat, wol, &wilB, -1);
		const float pdfB = mat.evalPdf(wilB, wol);
		const float pdfAsIfFromL = evalEnvMapPdf(surfPt, surfaceFrame, surfaceFrame.ToWorld(wilB), -1);
		const float misWeightB = misWeight(pdfB, pdfAsIfFromL);
		LoDirect += misWeightB * LoBRDF;
				
				
		for (int i = 0; i < mScene.GetLightCount(); ++i)
		{
			auto light = mScene.GetLightPtr(i);
			if (light->isDelta())
			{
				LoDirect += sampleLight(i, surfaceFrame, surfPt, wol, mat, nullptr);
				continue;
			}

			//sample light
			Vec3f wigL;
			const Vec3f LoLights = sampleLight(i, surfaceFrame, surfPt, wol, mat, &wigL);
			const float pdfL = evalEnvMapPdf(surfPt, surfaceFrame, wigL, i);
			const float pdfAsIfFromB = mat.evalPdf(surfaceFrame.ToLocal(wigL), wol);
			const float misWeightL = misWeight(pdfL, pdfAsIfFromB);
			LoDirect += misWeightL * LoLights;
		}

		if (oWil) *oWil = wilB;
		return LoDirect;
	}

	void directMis(const Vec2f sample, Ray ray, Vec3f wil)
	{
		
		Isect isect;
		isect.dist = 1e36f;

		if (mScene.Intersect(ray, isect))
		{
			Vec3f LoDirect = Vec3f(0);

			if (isect.lightID >= 0)
			{
				//intersecting light
				const AbstractLight* light = mScene.GetLightPtr(isect.lightID);
				float dotLN = Dot(isect.normal, -ray.dir);
				if (light && dotLN > 0)
				{
					LoDirect += light->emission();
				}
			}
			else
			{
				Frame surfaceFrame;
				surfaceFrame.SetFromZ(isect.normal);

				const Vec3f surfPt = ray.org + ray.dir * isect.dist;
				const Vec3f wol = surfaceFrame.ToLocal(-ray.dir);
				const Material& mat = mScene.GetMaterial(isect.matID);

				LoDirect += sampleMis(surfaceFrame, surfPt, wol, mat);
			}


			mFramebuffer.AddColor(sample, LoDirect);
		}
	}
	
	Vec3f generateBrdfSample(const Material& mat, const Vec3f& wol)
	{
		auto wil = mat.sampleDirL(wol, mRng);
		return wil;
	}

	//reflection
	Vec3f sampleBrdf(const Frame& surfaceFrame, const Vec3f& surfPt, const Material& mat, const Vec3f& wol, Vec3f* oWil,
	                 int lightId, Ray* oRay = nullptr)
	{
		//sample direction
		auto wil = generateBrdfSample(mat, wol);
		//auto wil = SampleCosHemisphereW(mRng.GetVec2f(), nullptr);
		auto wig = surfaceFrame.ToWorld(wil);
		if (oWil)
			*oWil = wil;

		//ray reflected from surface and new intersetction
		Ray ray(surfPt, wig, EPS_RAY);
		if (oRay) *oRay = ray;
		
		Isect isect(1e36f);

		if (mScene.Intersect(ray, isect))
		{
			if ((isect.lightID >= 0) && (isect.lightID == lightId || lightId == -1))
			{
				const AbstractLight* lightR = isect.lightID < 0 ? nullptr : mScene.GetLightPtr(isect.lightID);

				float dotLN = Dot(isect.normal, -ray.dir);
				if (lightR && dotLN > 0)
				{
					auto cos = Dot(Normalize(wil), {0, 0, 1});
					//LoDirect +=	lightR->emission() * mat.evalBrdf(wil, wol) * cos / mat.evalPdf(wil, wol);
					return lightR->emission() * mat.evalBrdf(wil, wol) * cos / mat.evalPdf(wil, wol);
				}
			}
		}
		else if (auto backgroundLight = mScene.GetBackground())
		{
			//environment light
			auto cos = Normalize(wil).z;
			return backgroundLight->emission() * mat.evalBrdf(wil, wol) * cos / mat.evalPdf(wil, wol);
		}
		else
		{
			return Vec3f(0.f);
		}
		return Vec3f(0.f);
	}

	Vec3f sampleLights(Frame surfaceFrame, const Vec3f surfPt, const Vec3f wol, const Material& mat)
	{
		Vec3f LoDirect(0.f);
		//iterate over lights

		//reflected light

		for (int i = 0; i < mScene.GetLightCount(); i++)
		{
			LoDirect += sampleLight(i, surfaceFrame, surfPt, wol, mat);
		}
		return LoDirect;
	}

	Vec3f sampleLight(int i, Frame surfaceFrame, const Vec3f surfPt, const Vec3f wol, const Material& mat,
	                  Vec3f* oWig = nullptr)
	{
		const AbstractLight* light = mScene.GetLightPtr(i);
		assert(light != 0);

		Vec3f wig;
		float lightDist;
		Vec3f illum = light->sampleIllumination(surfPt, surfaceFrame, wig, lightDist, mRng.GetVec2f());

		auto wil = surfaceFrame.ToLocal(wig);
		if (oWig)
			*oWig = wig;

		if (illum.Max() > 0)
		{
			if (!mScene.Occluded(surfPt, wig, lightDist))
			{
				return illum * mat.evalBrdf(wil, wol);
			}
		}
		return Vec3f(0.f);
	}


	float evalEnvMapPdf(const Vec3f surfPt, const Frame& frame, const Vec3f& wig, int lightId)
	{
		//ray reflected from surface and new intersection
		Ray ray(surfPt, wig, EPS_RAY);
		Isect isect(1e36f);

		if (mScene.Intersect(ray, isect))
		{
			if ((isect.lightID >= 0) && (isect.lightID == lightId || lightId == -1))
			{
				auto cos = Dot(Normalize(-wig), isect.normal);
				auto dist = isect.dist;
				const AbstractLight* light = mScene.GetLightPtr(isect.lightID);
				return light->pdfW(dist, cos);
			}
		}
		else if (auto backgroundLight = mScene.GetBackground())
		{
			return CosHemispherePdfW(frame.Normal(), frame.ToLocal(wig));
		}

		return 1.f;
	}


	float misWeight(const float pdfA, const float pdfAsIfFromB)
	{
		return pdfA / (pdfA + pdfAsIfFromB);
	}

	Rng mRng;
};
