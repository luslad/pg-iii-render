 [ -s <scene_id> | -a <algorithm> |
           -t <time> | -i <iteration> | -o <output_name> | --report ]

    -s  Selects the scene (default 0):
          0    walls spheres + point light + sph. diffuse walls diffuse
          1    walls spheres + point light + sph. diffuse sph. glossy walls diffuse walls glossy
          2    walls spheres + ceiling light + sph. diffuse walls diffuse
          3    walls spheres + ceiling light + sph. diffuse sph. glossy walls diffuse walls glossy
          4    walls spheres + light box + sph. diffuse walls diffuse
          5    walls spheres + light box + sph. diffuse sph. glossy walls diffuse walls glossy
          6    walls spheres + env. light + sph. diffuse walls diffuse
          7    walls spheres + env. light + sph. diffuse sph. glossy walls diffuse walls glossy
    -a  Selects the rendering algorithm (default vcm):
          el   eye light
          pt   path tracing
    -t  Number of seconds to run the algorithm
    -i  Number of iterations to run the algorithm (default 1)
    -o  User specified output name, with extension .bmp or .hdr (default .bmp)

    Note: Time (-t) takes precedence over iterations (-i) if both are defined